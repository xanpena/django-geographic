from django.urls import path

from countries.views import CountryDetailView, CountryDetailIDView, CountrySearchView

app_name = 'countries'

urlpatterns = [
    path('<int:pk>', CountryDetailIDView.as_view(), name='country_id'),
    path('<code>', CountryDetailView.as_view(), name='countries_code'),
    path('<search>/<query>', CountrySearchView.as_view(), name='countries_search'),
]
