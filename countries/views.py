from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
#from django.views.generic import View
from django.views.generic import TemplateView
from countries.models import Country
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

# Create your views here.
#def home(request):
    #return HttpResponse("hello world")
   # return render(request, "countries/home.html")


#class HomeView(View):
class HomeView(TemplateView):
    template_name = 'countries/home.html'

    #def get_context_data(self, *args, **kwargs):

        #colombia = {'name': 'colombia', 'code': 'CO'}
        #usa = {'name': 'estados unidos', 'code': 'USA'}
        #mexico = {'name': 'mexico', 'code': 'MX'}

        #countries = ['Italia', 'Francia', 'Portugal' ]
        #countries2 = [colombia, usa, mexico]
        #return {'countries': countries, 'countries2': countries2}
    #def get(self, request, *args, **kwargs):
    #    return render(request, "countries/home.html")

class CountryDetailIDView(DetailView):
    template_name = 'countries/country_detail_id.html'
    model = Country

    def get_context_data(self, *args, **kwargs):
        
        #id = kwargs['id']
        #return {'id': id}
        #try:
        #    country = Country.objects.get(id=kwargs['id'])
        #except Country.DoesNotExist as e:
        #    raise Http404()
        
        get_object_or_404(Country, id=kwargs['id'])

        return {'country': country}

class CountryDetailView(TemplateView):
    template_name = 'countries/country_detail.html'

    def get_context_data(self, *args, **kwargs):
        code = kwargs['code']
        return {'code': code}


class TagsView(TemplateView):
    template_name = 'countries/tags.html'


class CountrySearchView(ListView):
    template_name = 'countries/search.html'
    model = Country

    def get_queryet(self):
        query = self.kwargs['query']
        #return Country.objects.filter(name='Colombia')
        #return Country.objects.filter(name__startswith='co')
        #return Country.objets.filter(continent__name='Europe')
        #return Country.objets.filter(continent__name__contains='a')
        return Country.objets.filter(name__contains=query)
