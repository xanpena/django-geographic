from django.urls import reverse
from countries.models import Country
#def countries_data(request):
#    colombia = {'id':1, 'name': 'colombia', 'code': 'CO', 'detail_url': reverse('countries:country_id', kwargs={'id':1})}
#    usa = {'id':2, 'name': 'estados unidos', 'code': 'USA', 'detail_url': reverse('countries:country_id', kwargs={'id':2})}
#    mexico = {'id':3, 'name': 'mexico', 'code': 'MX', 'detail_url': reverse('countries:country_id', kwargs={'id':3})}
#
#    countries = [colombia, usa, mexico]
#    return {'countries': countries}

def countries_data(request):
    countries = Country.objects.filter(continent__name="asia")
    return {'countries': countries}