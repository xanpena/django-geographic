from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.
class ContinentsView(TemplateView):
    template_name = 'continents/continents.html'

    def get_context_data(self, *args, **kwargs):

        america = {'name': 'America', 'color': 'yellow'}
        africa = {'name': 'Africa', 'color': 'dark'}
        europe = {'name': 'Europe', 'color': 'blue'}
        continents = [america, africa, europe]
        return {'continents': continents}