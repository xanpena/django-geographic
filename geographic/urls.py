"""geographic URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
#from countries.views import home
#from countries.views import HomeView
#from django.views.generic import TemplateView
from countries.views import (
    HomeView, TagsView, #CountryDetailView, CountryDetailIDView
)
from continents.views import ContinentsView

app_name = 'geographics'

urlpatterns = [
    path('admin/', admin.site.urls),
    #path("", home)
    #path("", HomeView.as_view())
    #path("", TemplateView.as_view(template_name = 'countries/home.html'))
    path("", HomeView.as_view(), name='home'),
    path("tags", HomeView.as_view(), name='tags'),
    path("continents/", include("continents.urls", namespace="continents")),
    path('countries/', include("countries.urls", namespace="countries")),
    path('people/', include("people.urls", namespace="people")),
    #path('countries/<code>', CountryDetailView.as_view(), name='countries_code')
    ]